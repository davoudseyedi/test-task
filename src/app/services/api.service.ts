import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()

export class ApiService{

  constructor(private http: HttpClient) {
  }

  public getPostList(){
    return this.http.get('https://jsonplaceholder.org/posts')
  }

}
