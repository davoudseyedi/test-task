import {Component, OnInit} from '@angular/core';
import {ApiService} from "./services/api.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  public scrollDistance = 1;
  public scrollUpDistance = 2;
  public throttle = 5;

  public number = 5;
  public sort='';

  public postList : any[] = [];

  public call: any = null;

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {

    this.getPostS();

  }

  public onScrollDown() {
    clearTimeout(this.call);
    this.call = setTimeout(() => {
      this.number += 5
    }, 500);

  }

  public onScrollUp() {

    this.number -= 5;

  }

  public sortList(){
    if(this.sort == 'a-z'){

      this.postList.sort((a,b) => a.title > b.title ? 1 : -1);

    }else if(this.sort == 'z-a'){

      this.postList.sort((a,b) => a.title > b.title ? -1 : 1);

    }
  }

  private getPostS() {

    this.apiService.getPostList().subscribe({
      next: this.getHomeDataSuccess.bind(this),
      error: this.getHomeDataError.bind(this),
    });

  }


  private getHomeDataSuccess(response: any) {

    this.postList = response

  }

  private getHomeDataError(error: any) {
    console.log(error)
  }
}


